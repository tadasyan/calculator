package GUI;

import java.awt.*;
import java.awt.event.ActionEvent;

import Engine.*;

import javax.swing.*;

public class CalculatorGUI {

	private JPanel windowContent; 
	private JTextField display;
	private JButton bPlus;
	private JButton bMinus;
	private JButton bMult;
	private JButton bDiv;
	private JButton bEqually;
	private JButton bClear;

//	array of buttons 0-9 and .
	private JButton[] b;
	
    public void setDisplayValue(String val){
        display.setText(val);
    }

    public String getDisplayValue() {
        return display.getText();
    }
	
//method for creating GridLayout and adding buttons
	private JButton setGBL(JPanel panel, GridBagConstraints c, JButton comp, int gridX, int gridY, String name){
		comp = new JButton(name);
		c.gridx = gridX;
		c.gridy = gridY;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.fill = c.BOTH;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.anchor = c.CENTER;
		panel.add(comp, c);
	 	return comp;
	}
	
	CalculatorGUI(){
		b = new JButton[11];
		windowContent = new JPanel();
		windowContent.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
	//creating and adding textField 		
		display = new JTextField();
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 3;
		c.fill = c.BOTH;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.anchor = c.CENTER;
		windowContent.add(display, c);
		
		b[1] = setGBL(windowContent, c, b[1], 0, 1, "1");
		b[2] = setGBL(windowContent, c, b[2], 1, 1, "2");
		b[3] = setGBL(windowContent, c, b[3], 2, 1, "3");
		b[4] = setGBL(windowContent, c, b[4], 0, 2, "4");
		b[5] = setGBL(windowContent, c, b[5], 1, 2, "5");
		b[6] = setGBL(windowContent, c, b[6], 2, 2, "6");
		b[7] = setGBL(windowContent, c, b[7], 0, 3, "7");
		b[8] = setGBL(windowContent, c, b[8], 1, 3, "8");
		b[9] = setGBL(windowContent, c, b[9], 2, 3, "9");
		b[0] = setGBL(windowContent, c, b[0], 0, 4, "0");
		b[10] = setGBL(windowContent, c, b[10], 1, 4, ".");
		bClear = setGBL(windowContent, c, bClear, 3, 0, "CLR");
		bEqually = setGBL(windowContent, c, bEqually, 2, 4, "=");
		bPlus = setGBL(windowContent, c, bPlus, 3, 1, "+");
		bMinus = setGBL(windowContent, c, bMinus, 3, 2, "-");
		bMult = setGBL(windowContent, c, bMult, 3, 3, "*");
		bDiv = setGBL(windowContent, c, bDiv, 3, 4, "/");
	
//  add listeners
		for (int i=0; i<b.length; i++){
			b[i].addActionListener(
					new CalculatorEngine(this){
						public void actionPerformed(ActionEvent e){
							// Get the source of this action
							JButton clickedButton =  (JButton) e.getSource();
							   	
							// Get the existing text from the Calculator�s
							// display field. Reaching inside another object is bad.
							String dispFieldText = getParent().getDisplayValue();
							   
							// Get the button's label 
							String clickedButtonLabel = clickedButton.getText();
							
							if (dispFieldText.equals("") && clickedButtonLabel.equals(".")){
								getParent().setDisplayValue(dispFieldText + "0" + clickedButtonLabel);
							}
							else if ((clickedButtonLabel.equals(".") && !getPointFlag()) ||
									(!clickedButtonLabel.equals("."))){						
								getParent().setDisplayValue(dispFieldText + clickedButtonLabel);
							}
							if (clickedButtonLabel.equals(".")) setPointFlag();
						}
					});
		}
		bPlus.addActionListener(
				new CalculatorEngine(this){
					public void actionPerformed(ActionEvent e){
						//  remember operation, displayValue and clear it 
						doOperation("+");
					}
				});
		bMinus.addActionListener(
				new CalculatorEngine(this){
					public void actionPerformed(ActionEvent e){
						//  remember operation, displayValue and clear it 
						doOperation("-");
					}
				});
		bMult.addActionListener(
				new CalculatorEngine(this){
					public void actionPerformed(ActionEvent e){
						//  remember operation, displayValue and clear it 
						doOperation("*");
					}
				});
		bDiv.addActionListener(
				new CalculatorEngine(this){
					public void actionPerformed(ActionEvent e){
						//  remember operation, displayValue and clear it 
						doOperation("/");
					}
				});
		bEqually.addActionListener(
				new CalculatorEngine(this){
					public void actionPerformed(ActionEvent e){
						if (CalculatorEngine.operation == "+"){
							double temp = sum(Double.parseDouble(getParent().getDisplayValue()));
							getParent().setDisplayValue(String.valueOf(temp));
						}
						else if (CalculatorEngine.operation == "-"){
							double temp = difference(Double.parseDouble(getParent().getDisplayValue()));
							getParent().setDisplayValue(String.valueOf(temp));
						}
						else if (CalculatorEngine.operation == "*"){
							double temp = multiplication(Double.parseDouble(getParent().getDisplayValue()));
							getParent().setDisplayValue(String.valueOf(temp));
						}
						else if (CalculatorEngine.operation == "/"){
							double temp = division(Double.parseDouble(getParent().getDisplayValue()));
							getParent().setDisplayValue(String.valueOf(temp));
						}
					}
				});
		bClear.addActionListener(
				new CalculatorEngine(this){
					public void actionPerformed(ActionEvent e){
						getParent().setDisplayValue("");
					}
				});
		//Create and set up the window.
        JFrame frame = new JFrame("Calculator");
        frame.setContentPane(windowContent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
      //Display the window.
      //  frame.pack();
        frame.setSize(250, 250);
        frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		new CalculatorGUI();
		

	}

}
