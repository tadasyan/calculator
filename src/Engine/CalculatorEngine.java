package Engine;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import GUI.*;

public class CalculatorEngine implements ActionListener { 
	private CalculatorGUI parent;
	private boolean pointFlag;
	public static String operation;
	private static double value;
	
	public void doOperation (String operation){
		CalculatorEngine.operation = operation;
		value = Double.parseDouble(parent.getDisplayValue());
		parent.setDisplayValue("");
	}
	
	public double sum (double number){
		return value + number;
	}
	
	public double difference (double number){
		return value - number;
	}
	
	public double multiplication (double number){
		return value * number;
	}
	
	public double division (double number){
		return value / number;
	}
	
	public boolean getPointFlag(){
		return pointFlag;
	}
	
	public void setPointFlag(){
		this.pointFlag = true;
	}
	
	public CalculatorGUI getParent(){
		return parent;
	}
	
	public CalculatorEngine(CalculatorGUI parent){
		this.parent = parent;
		this.pointFlag = false;
	}
	
	public void actionPerformed(ActionEvent e){
	}
}
